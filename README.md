![Maintenance](https://img.shields.io/badge/maintenance-experimental-blue.svg)

# dp3t

License: MIT


[Latest nuget builds](https://gitlab.com/PrivateTracer/dp3t-rust/-/jobs/artifacts/master/download?job=deploy)

## Prerequisites

Have Rust and Cargo installed ([rust-lang.org - Getting Started](https://www.rust-lang.org/learn/get-started))

## Generating C headers

In order to generate a C header for this project you need to have cbindgen installed:

```
$ cargo install cbindgen
```

After that you can request a header file to be made by calling:

```
$ cbindgen -l C -o target/libdp3t.h
```

## Generating output for iOS

In order to generate libraries for iOS you need to have the iOS toolchain installed:

```
$ rustup target add aarch64-apple-ios // ARM64  (For real device)
$ rustup target add x86_64-apple-ios  // X64    (For simulator)
```

You can then compile both the targets as follows:

```
$ cargo build --release --target aarch64-apple-ios
$ cargo build --release --target x86_64-apple-ios
```

You can then combine both the static libraries into a universal library using `lipo`:

```
$ lipo -create target/aarch64-apple-ios/release/libdp3t.a target/x86_64-apple-ios/release/libdp3t.a -output target/libdp3t.a
```

> NB: If you want to create a debug build for the library, replace `release` with `debug` in the two steps above.

## Cross builds

We use [Cross](https://github.com/rust-embedded/cross) for cross
platform builds. Currently, this is only required for the server
component.

```
cargo install cross
cargo install --git https://github.com/jellelicht/cargo-nuget.git
```

> Cargo-nuget uses a fork that hardcodes the supported
> NETCore.Platforms to 3.1.0 for now. Someone should make this
> configurable in cargo-nuget instead

Then:
```
cross build  --target=x86_64-pc-windows-gnu
cross build  --target=x86_64-unknown-linux-gnu
cargo-nuget cross --targets win-x64 linux-x64 --win-x64-path ./target/x86_64-pc-windows-gnu/debug/dp3t.dll --linux-x64-path ./target/x86_64-unknown-linux-gnu/debug/libdp3t.so
```

To read artifacts from our nuget repo, run:
```
nuget source Add -Name "mygitlab-nuget-repo" -Source "https://gitlab.com/api/v4/projects/18102905/packages/nuget/index.json"
```

Sadly, Gitlab CI is currently not allowed to do this with `CI_JOB_TOKEN`, so using a personal token is all we can do for now.
A release can subsequently be made by:
```
nuget source Add -Name "mygitlab-nuget-repo" -Source "https://gitlab.com/api/v4/projects/18102905/packages/nuget/index.json" -UserName "<yourgitlab-username>" -Password "<your gitlab api token>"
nuget push <package_file> -Source "mygitlab-nuget-repo"
```
