extern crate cuckoofilter;
extern crate serde_json;

pub const EPHID_SEED_LENGTH: usize = 32;
pub const EPHID_ID_LENGTH: usize = 16;
pub const EPHID_HASH_LENGTH: usize = 256 / 8;
/// Length of a single epoch in minutes
pub const EPOCH_LENGTH: i64 = 5;
pub type Epoch = i32;
/*
   Should actually be std::mem::sizeof::<i32>(), but the bindgen won't shut up about it,
   presumably because C types aren't stable sizes like rust, they depend on the platform.
*/
pub const ID_TIME_ENTRY_LENGTH: usize = EPHID_ID_LENGTH + 4;
pub type EphemeralSeed = [u8; EPHID_SEED_LENGTH];
pub type EphemeralId = [u8; EPHID_ID_LENGTH];
pub type IdTimeEntry = [u8; ID_TIME_ENTRY_LENGTH];
pub type HashedIdentity = [u8; EPHID_HASH_LENGTH];
