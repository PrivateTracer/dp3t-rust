use crate::types::*;
use std::convert::TryInto;
use std::time::{SystemTime, UNIX_EPOCH};

#[repr(C)]
pub struct EpochInfo {
    /// Current epoch id
    pub id: Epoch,
    /// Request id again after this unix timestamp
    pub next_after: i64,
}

impl EpochInfo {
    pub fn new() -> Self {
        let duration = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("Negative time?");
        let ts: i64 = duration.as_secs().try_into().unwrap();
        EpochInfo::from_timestamp(ts)
    }

    pub fn from_timestamp(ts: i64) -> Self {
        let id = ts / 60 / EPOCH_LENGTH;

        let next_after = (id + 1) * EPOCH_LENGTH * 60;

        EpochInfo {
            id: id as i32,
            next_after,
        }
    }
}

/// Constructs a new EpochInfo struct using the current unix timestamp as base.
///
/// You don't have to free this
#[no_mangle]
pub extern "C" fn EPOCH_INFO_new() -> EpochInfo {
    EpochInfo::new()
}

/// Constructs a new EpochInfo struct using the provided unix timestamp as base.
///
/// You don't have to free this
#[no_mangle]
pub extern "C" fn EPOCH_INFO_from_timestamp(ts: i64) -> EpochInfo {
    EpochInfo::from_timestamp(ts)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn from_timestamp_zero_test() {
        let ts = 0;

        let info = EpochInfo::from_timestamp(ts);

        assert_eq!(0, info.id);
        // Should be 5 minutes (300 seconds for those who are bad at time math) later.
        assert_eq!(300, info.next_after);
    }

    #[test]
    fn from_timestamp_2020_test() {
        // April 15 2020, 18:50:40 UTC a totally random date and entirely not
        // when I wrote this test.
        let ts = 1586976640;

        let info = EpochInfo::from_timestamp(ts);

        assert_eq!(5289922, info.id);
        // Should be April 15 2020, 18:55:00 UTC aka, next 5 minute window
        assert_eq!(1586976900, info.next_after);
    }

    #[test]
    fn from_timestamp_edge_test() {
        let ts = 1586976900;
        let info = EpochInfo::from_timestamp(ts);
        assert_eq!(5289923, info.id);
        assert_eq!(1586977200, info.next_after);
    }
}
