extern crate cuckoofilter;
extern crate serde_json;

use base64::{decode, encode};
use cuckoofilter::{CuckooError, CuckooFilter, ExportedCuckooFilter};
use deflate::deflate_bytes;
use inflate::inflate_bytes;
use std::collections::hash_map::DefaultHasher;
use std::ffi::{CStr, CString};
use std::os::raw::c_char;
use std::str;

use crate::types::*;

pub struct SimpleCuckooFilter {
    filter: CuckooFilter<DefaultHasher>,
}

impl SimpleCuckooFilter {
    pub fn new() -> Self {
        SimpleCuckooFilter {
            filter: CuckooFilter::new(),
        }
    }

    pub fn with_capacity(cap: usize) -> Self {
        SimpleCuckooFilter {
            filter: CuckooFilter::<DefaultHasher>::with_capacity(cap),
        }
    }

    pub fn serialize(&self) -> Result<String, String> {
        let store: ExportedCuckooFilter = self.filter.export();
        serde_json::to_string(&store).map_err(|e| e.to_string())
    }

    pub fn serialize_blob(&self) -> Result<String, String> {
        self.serialize()
            .map(|json| encode(deflate_bytes(json.as_bytes())))
    }

    pub fn deserialize(json_string: &str) -> Result<Self, String> {
        match serde_json::from_str::<ExportedCuckooFilter>(json_string) {
            Ok(s) => Ok(SimpleCuckooFilter {
                filter: CuckooFilter::<DefaultHasher>::from(s),
            }),
            Err(e) => Err(e.to_string()),
        }
    }

    pub fn deserialize_blob(blob_string: &str) -> Result<Self, String> {
        decode(blob_string)
            .map_err(|e| e.to_string())
            .and_then(|v| inflate_bytes(&v))
            .and_then(|v| String::from_utf8(v).map_err(|e| e.to_string()))
            .and_then(|s| SimpleCuckooFilter::deserialize(&s))
    }

    pub fn add(&mut self, x: &HashedIdentity) -> Result<(), CuckooError> {
        self.filter.add(x)
    }

    pub fn contains(&self, x: &HashedIdentity) -> bool {
        self.filter.contains(x)
    }
}

/// Creates a new CuckooFilter struct.
///
/// Struct needs to be freed by calling CF_free()
///
/// # Examples
/// ```
/// use dp3t::cuckoo::*;
///
/// let ptr = CF_new();
/// unsafe { CF_free(ptr) };
/// ```
#[no_mangle]
pub extern "C" fn CF_new() -> *mut SimpleCuckooFilter {
    Box::into_raw(Box::new(SimpleCuckooFilter::new()))
}

/// Creates a new CuckooFilter struct with the specified size.
///
/// Struct needs to be freed by calling CF_free()
///
/// # Examples
/// ```
/// use dp3t::cuckoo::*;
///
/// let ptr = CF_new_with_capacity(512);
/// unsafe { CF_free(ptr) };
/// ```
#[no_mangle]
pub extern "C" fn CF_new_with_capacity(cap: usize) -> *mut SimpleCuckooFilter {
    Box::into_raw(Box::new(SimpleCuckooFilter::with_capacity(cap)))
}

/// Frees the CuckooFilter
///
/// # Arugments
///
/// `ptr` - Pointer to CuckooFilter struct.
#[no_mangle]
pub unsafe extern "C" fn CF_free(ptr: *mut SimpleCuckooFilter) {
    drop(Box::from_raw(ptr));
}

/// Packs the CuckooFilter in a Json string. Returns 0 on success, returns -1 on failure.
///
/// The string that is created in `out` needs to be freed by calling CString_free().
///
/// # Arguments
///
/// `ptr` - Pointer to the CuckooFilter struct
/// `out` - Pointer where the C style json string will be located at if this function succeeds.
#[no_mangle]
pub unsafe extern "C" fn CF_serialize(
    ptr: *const SimpleCuckooFilter,
    out: *mut *mut c_char,
) -> i32 {
    match (&*ptr).serialize() {
        Ok(r) => {
            let cstring = CString::new(r).unwrap().into_raw();
            out.write(cstring);
            0
        }
        Err(_) => -1,
    }
}

/// Packs the CuckooFilter in a base64-encoded Capsid. Returns 0 on success, returns -1 on failure.
///
/// The string that is created in `out` needs to be freed by calling CString_free().
///
/// # Arguments
///
/// `ptr` - Pointer to the CuckooFilter struct
/// `out` - Pointer where the C style base64-encoded string will be located at if this function succeeds.
#[no_mangle]
pub unsafe extern "C" fn CF_serialize_blob(
    ptr: *const SimpleCuckooFilter,
    out: *mut *mut c_char,
) -> i32 {
    match (&*ptr).serialize_blob() {
        Ok(r) => {
            let cstring = CString::new(r).unwrap().into_raw();
            out.write(cstring);
            0
        }
        Err(_) => -1,
    }
}

/// Constructs a new CuckooFilter struct from a json string. Returns 0 on success
/// returns any small negative number on failure.
///
/// The filter constructed using this function needs to be freed when you're done
/// using it by calling CF_free().
///
/// # Arguments
///
/// `json_string` - C style string (0 terminated) containing the json.
/// `out` - Out pointer will contain pointer to the CuckooFilter on success.
#[no_mangle]
pub extern "C" fn CF_deserialize(
    json_string: *const c_char,
    out: *mut *mut SimpleCuckooFilter,
) -> i32 {
    let perhaps_json_string = unsafe { CStr::from_ptr(json_string) }.to_str();
    if let Ok(json_string) = perhaps_json_string {
        match SimpleCuckooFilter::deserialize(json_string) {
            Ok(cf) => {
                unsafe { out.write(Box::into_raw(Box::new(cf))) };
                0
            }
            Err(_) => -1,
        }
    } else {
        -2
    }
}

/// Constructs a new CuckooFilter struct from a base64-encoded Capsid
/// string. Returns 0 on success returns any small negative number on failure.
///
/// The filter constructed using this function needs to be freed when you're done
/// using it by calling CF_free().
///
/// # Arguments
///
/// `json_string` - C style string (0 terminated) containing the json.
/// `out` - Out pointer will contain pointer to the CuckooFilter on success.
#[no_mangle]
pub extern "C" fn CF_deserialize_blob(
    base64_string: *const c_char,
    out: *mut *mut SimpleCuckooFilter,
) -> i32 {
    let perhaps_base64_string = unsafe { CStr::from_ptr(base64_string) }.to_str();
    if let Ok(base64_string) = perhaps_base64_string {
        match SimpleCuckooFilter::deserialize_blob(base64_string) {
            Ok(cf) => {
                unsafe { out.write(Box::into_raw(Box::new(cf))) };
                0
            }
            Err(_) => -1,
        }
    } else {
        -2
    }
}

/// Adds an entry to the Cuckoo filter. Returns 0 on success, returns -1 when
/// there is not enough space in the filter.
///
/// # Arguments
///
/// `ptr` - Pointer to CuckooFilter struct.
/// `x` - 32 byte array of the hashed identity.
#[no_mangle]
pub unsafe extern "C" fn CF_add(ptr: *mut SimpleCuckooFilter, x: &HashedIdentity) -> i32 {
    let cf = &mut *ptr;

    match cf.add(x) {
        Ok(_) => 0,
        Err(_) => -1,
    }
}

/// Checks if the time entry is in the Cuckoo Filter. Returns true if it is, false if not.
///
/// # Arguments
///
/// `ptr` - Const pointer to the CuckooFilter struct.
/// `x` - 32 byte array of the hashed identity.
#[no_mangle]
pub unsafe extern "C" fn CF_contains(ptr: *const SimpleCuckooFilter, x: &HashedIdentity) -> bool {
    let cf = &*ptr;

    cf.contains(x)
}

#[cfg(test)]
mod tests {
    use super::*;
    // Note: this test code leaks some CStrings right now.

    #[test]
    fn cf_new_test() {
        let cf = CF_new();

        let mut s = std::mem::MaybeUninit::uninit();
        let status = unsafe { CF_serialize(cf, s.as_mut_ptr()) };
        let string = unsafe { CString::from_raw(*s.as_mut_ptr()) }
            .into_string()
            .unwrap();
        unsafe { CF_free(cf) };

        let cf = SimpleCuckooFilter::new();
        let normal = cf.serialize().unwrap();

        assert_eq!(0, status);
        assert_eq!(normal, string);
    }

    #[test]
    fn cf_new_blob_test() {
        let cf = CF_new();

        let mut s = std::mem::MaybeUninit::uninit();
        let status = unsafe { CF_serialize_blob(cf, s.as_mut_ptr()) };
        let string = unsafe { CString::from_raw(*s.as_mut_ptr()) }
            .into_string()
            .unwrap();

        unsafe { CF_free(cf) };
        let cf = SimpleCuckooFilter::new();
        let normal = cf.serialize_blob().unwrap();

        assert_eq!(0, status);
        assert_eq!(normal, string);
    }

    #[test]
    fn cf_serde_test() {
        let cf = CF_new();
        let mut s = std::mem::MaybeUninit::uninit();
        // Fill a few values.
        for i in 0u8..50 {
            let status = unsafe { CF_add(cf, &[i; EPHID_HASH_LENGTH]) };
            assert_eq!(0, status);
        }

        let status = unsafe { CF_serialize(cf, s.as_mut_ptr()) };
        assert_eq!(0, status);

        unsafe { CF_free(cf) };
        let mut ccf = std::mem::MaybeUninit::uninit();

        let string = unsafe { CString::from_raw(*s.as_mut_ptr()) };
        let status = CF_deserialize(string.as_ptr(), ccf.as_mut_ptr());
        assert_eq!(0, status);

        for i in 0u8..50 {
            assert!(unsafe { CF_contains(*ccf.as_ptr(), &[i; EPHID_HASH_LENGTH]) });
        }

        unsafe { CF_free(*ccf.as_mut_ptr()) };
    }

    #[test]
    fn cf_serde_blob_test() {
        let cf = CF_new();
        let mut s = std::mem::MaybeUninit::uninit();
        // Fill a few values.
        for i in 0u8..50 {
            let status = unsafe { CF_add(cf, &[i; EPHID_HASH_LENGTH]) };
            assert_eq!(0, status);
        }

        let status = unsafe { CF_serialize_blob(cf, s.as_mut_ptr()) };
        assert_eq!(0, status);

        unsafe { CF_free(cf) };
        let mut ccf = std::mem::MaybeUninit::uninit();

        let string = unsafe { CString::from_raw(*s.as_mut_ptr()) };
        let status = CF_deserialize_blob(string.as_ptr(), ccf.as_mut_ptr());
        assert_eq!(0, status);

        for i in 0u8..50 {
            assert!(unsafe { CF_contains(*ccf.as_ptr(), &[i; EPHID_HASH_LENGTH]) });
        }

        unsafe { CF_free(*ccf.as_mut_ptr()) };
    }

    #[test]
    fn serialization() {
        // Just a small filter to test serialization.
        let mut filter = SimpleCuckooFilter::with_capacity(100);

        // Fill a few values.
        for i in 0u8..50 {
            filter.add(&[i; EPHID_HASH_LENGTH]).unwrap();
        }

        let saved_json = filter.serialize().unwrap();
        let recovered_filter = SimpleCuckooFilter::deserialize(&saved_json).unwrap();

        // Check our values exist within the reconstructed filter.
        for i in 0u8..50 {
            assert!(recovered_filter.contains(&[i; EPHID_HASH_LENGTH]));
        }
    }

    #[test]
    fn blob_serialization() {
        // Just a small filter to test serialization.
        let mut filter = SimpleCuckooFilter::with_capacity(100);

        // Fill a few values.
        for i in 0u8..50 {
            filter.add(&[i; EPHID_HASH_LENGTH]).unwrap();
        }

        let saved_blob = filter.serialize_blob().unwrap();
        let recovered_filter = SimpleCuckooFilter::deserialize_blob(&saved_blob).unwrap();

        // Check our values exist within the reconstructed filter.
        for i in 0u8..50 {
            assert!(recovered_filter.contains(&[i; EPHID_HASH_LENGTH]));
        }
    }

    #[test]
    fn sanity_test() {
        let value = b"Hello world";
        let mut cf = CuckooFilter::new();

        let success = cf.add(value);
        assert!(success.is_ok());

        // Lookup if data is in the filter
        let success = cf.contains(value);
        assert!(success);

        // Test and add to the filter (if data does not exists then add)
        let success = cf.test_and_add(value);
        assert!(success.is_ok());
        assert_eq!(success.unwrap(), false);

        // Remove data from the filter.
        let success = cf.delete(value);
        assert!(success);

        // Lookup if data is in the filter
        let success = cf.contains(value);
        assert!(!success);
    }
}
