use ignore_result::Ignore;
use std::io::{self, Write};
extern crate dp3t;

fn main() {
    for _ in 0..10000 {
        let seed = dp3t::generate_eph_seed();
        let ephid = dp3t::generate_eph_ephid(&seed);
        io::stdout().write_all(&ephid).ignore();
    }
}
