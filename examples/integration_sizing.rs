extern crate dp3t;

use cuckoofilter::CuckooError;
use deflate::deflate_bytes;
use dp3t::cuckoo::SimpleCuckooFilter;
use dp3t::types::*;
use rand::Rng;

fn try_add_random_entry(filter: &mut SimpleCuckooFilter) -> Result<(), CuckooError> {
    filter.add(&rand::thread_rng().gen::<[u8; EPHID_HASH_LENGTH]>())
}

fn fill_filter(filter: &mut SimpleCuckooFilter) -> usize {
    let mut amount = 0;
    while let Ok(_) = try_add_random_entry(filter) {
        amount = amount + 1;
    }
    amount
}
fn main() {
    let values = vec![1 << 3, 1 << 6, 1 << 9, 1 << 12, 1 << 15, 1 << 18, 1 << 21];

    for capacity in values.iter() {
        let mut filter = SimpleCuckooFilter::with_capacity(*capacity);
        let count = fill_filter(&mut filter);
        let serialized = filter.serialize().unwrap();
        let bytes = deflate_bytes(serialized.as_bytes());
        println!(
            "{:?} with {:?} gives {:?} bytes",
            capacity,
            count,
            bytes.len()
        );
    }
}
